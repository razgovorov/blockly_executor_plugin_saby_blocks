from blockly_executor import KeyNotFound
from blockly_executor.core.context import Context as ContextCore
from blockly_executor.core.exceptions import WorkspaceNotFound


class BlocklyContext(ContextCore):
    def __init__(self):
        super().__init__()
        self.service = None
        self.report = None

    @classmethod
    async def init(cls, *, debug_mode=None, current_block=None, current_workspace=None, workspace_name=None, data=None,
                   report=None, service=None, **kwargs):
        context_data = report.blockly_context if report else data
        _self = await super().init(debug_mode=debug_mode, current_block=current_block,
                                   current_workspace=current_workspace,
                                   workspace_name=workspace_name, data=context_data, **kwargs)
        _self.service = service
        _self.report = report
        return _self

    def init_nested(self, block_context, workspace_name):
        _self: BlocklyContext = super().init_nested(block_context, workspace_name)
        _self.service = self.service
        _self.report = self.report
        return _self

    async def workspace_read(self):
        try:
            action = await self.service.workspace_read(self.report.operation, self.workspace_name)
            return action.result['workspace']
        except KeyNotFound:
            raise WorkspaceNotFound(detail=self.workspace_name)
