""" Базовые методы взаимодействия Blockly со СБИС """
__author__ = 'vlasov.ii'

import json
import uuid

from sbis import RpcFile, Record, RecordSet, ExtSyncDoc as ExtSyncDocBl, MappingObject, Connector, API3, LogMsg, \
    LogLevel
from base64 import b64decode, b64encode
import time
from HelperSbis import HelperSbis
from IntegrationConfig.Connection import Connection
from ExtException import ExtException
from IntegrationSync.ExtSyncDoc import ExtSyncDoc
from ExtRequests import ExtRequests

LOG_LEVEL = LogLevel.llSTANDARD


class IntegrationHelper:
    """
    VII Класс с методами взаимодействия Blockly со СБИС.
    """

    # region Integration (Базовые методы, вызываемые с IntegrationConfig)
    @classmethod
    def connection_read(cls, _uuid):
        """
        VII Получение значения поля json_connection подключения по UUID'у
        :param _uuid: Идентификатор подключения
        :type _uuid: str
        :return: Значение поля json_connection подключения
        """
        return Connection().init_by_uuid(_uuid)
        # context_param = connection.json_connection.get('BlocklyExecutorContext', {})
        # LogMsg(LOG_LEVEL, f'Получение данных Blockly на подключении: {json.dumps(context_param, ensure_ascii=False)}')
        # return context_param

    @classmethod
    def connection_update(cls, _uuid, data):
        """
        VII Обновление значения поля json_connection подключения по UUID'у
        :param _uuid: Идентификатор подключения
        :type _uuid: str
        :param data: Данные для обновления поля json_connection
        :type data: dict
        :return: nothing
        """
        LogMsg(LOG_LEVEL, f'Обновленные данных Blockly на подключении: {json.dumps(data, ensure_ascii=False)}')
        connection = Connection().init_by_uuid(_uuid)
        connection.json_connection['BlocklyExecutorContext'] = data
        connection.update()

    @classmethod
    def report_read(cls, connection_uuid, report_uuid):
        """
        VII Получение значения поля Data записи ExtSyncDoс, полученной по идентификатору operation_uuid.
        Если запись по идентификатору не получена будет сгенерирована запись с переданным идентификатором.
        :param connection_uuid: Идентификатор подключения
        :type connection_uuid: str
        :param report_uuid: Идентификатор записи таблицы ExtSyncDoс (идентификатор операции)
        :type report_uuid: str
        :return: Значение поля Data записи ExtSyncDoс
        :rtype: dict
        """
        try:
            ext_sync_doc = ExtSyncDoc().init(connection_uuid=connection_uuid, sync_doc_uuid=report_uuid)
            ext_sync_doc.uuid = report_uuid
            ext_sync_doc.update()
            _data = ext_sync_doc.data['Data']
            return _data.get('blockly', {})
        except Exception as ex:
            raise ExtException(ex, action='IntegrationHelper.read_operation', dump={'connection_uuid': connection_uuid,
                                                                                    'report_uuid': report_uuid})

    @classmethod
    def report_update(cls, report_uuid, data):
        """
        VII Обновление значения поля Data записи ExtSyncDoc, получаемой по идентификатору
        :param report_uuid: Идентификатор записи таблицы ExtSyncDoс (идентификатор операции)
        :type report_uuid: str
        :param data: Данные для обновления поля Data
        :type data: dict
        :return: nothing
        """
        try:
            ext_sync_doc = ExtSyncDoc().init(sync_doc_uuid=report_uuid)
            _data = ext_sync_doc.data['Data']
            _data['blockly'] = data
            ext_sync_doc.data['Data'] = _data
            ext_sync_doc.update()
        except Exception as ex:
            raise ExtException(ex, action='IntegrationHelper.update_operation', dump={'operation_uuid': report_uuid,
                                                                                      'data': str(data)})

    @classmethod
    async def workspace_read(cls, connection, ini_name):
        """
        VII Чтение ини для подключения по названию
        :param connection_uuid: Идентификатор подключения
        :type connection_uuid: str
        :param ini_name: Название ини
        :type ini_name: str
        :param connection: экзумпляр класса подключения
        :type ini_name:
        :return: строка в формате XML содержащая описание операций
        :rtype: str
        """
        return connection.read_one_ini('Blockly', "Blockly_" + ini_name).get('data', '')

    # endregion

    # region Online (Методы, вызываемые с Online)
    @classmethod
    async def read_changes(cls, context, _filter):
        """ VII Получение списка изменений по фильтру """
        ep = HelperSbis.get_online_end_point()
        return ep.СБИС.СписокИзменений(_filter)

    @classmethod
    async def read_document(cls, context, document):
        """ Чтение объекта документ по параметрами """
        ep = HelperSbis.get_online_end_point()
        return ep.СБИС.ПрочитатьДокумент(document)

    @classmethod
    async def write_document(cls, context, document):
        """ Чтение объекта документ по параметрами """
        ep = HelperSbis.get_online_end_point()
        return ep.СБИС.ЗаписатьДокумент(document)

    @classmethod
    async def write_attachment(cls, context, document):
        """ Чтение объекта документ по параметрами """
        ep = HelperSbis.get_online_end_point()
        return ep.СБИС.ЗаписатьВложение(document)

    @classmethod
    async def task_list(cls, context, _filter):
        """ Получение списка задач """
        ep = HelperSbis.get_online_end_point()
        return ep.API3.TaskList(_filter)

    @classmethod
    async def prepare_action(cls, context, document):
        """ Команда подготовки действия на документе """
        ep = HelperSbis.get_online_end_point()
        return ep.СБИС.ПодготовитьДействие(document)

    @classmethod
    async def execute_action(cls, context, document):
        """ Команда выполнения действия на дкоументе """
        ep = HelperSbis.get_online_end_point()
        return ep.СБИС.ВыполнитьДействие(document)

    @classmethod
    async def fed_convert_xml_to_obj(cls, context, pattern, b64xml):
        """
        VII Получение подстановки для XML документа
        :param context:
        :param pattern:
        :param b64xml: Данные файла в формате base64
        :type b64xml: str
        :return: Подстановка для переданного документа
        :rtype: dict
        """
        ep = HelperSbis.get_online_end_point()
        return ep.Интеграция.ФЭДПолучитьПодстановку(pattern, b64xml)

    @classmethod
    async def fed_convert_obj_to_xml(cls, context, obj, xml_format, xml_version='', pattern='Генератор'):
        """
        VII Получение XML документа по подстановке
        :param context:
        :param params: Параметры формируемого из подстановки дкоумента
        :type params: dict
            Пример: {
                'ТипДокумента': '',
                'ПодТипДокумента': '',
                'ВерсияФормата': '',
                'ПодВерсияФормата': ''
            }
        :param substitution: Подстановка
        :type substitution: dict
        :return: Xml документ в формате base64
        :rtype: str
        """
        fed_type_subtype = xml_format.split('_')
        fed_version_subversion = xml_version.split('_')
        if not obj:
            raise Exception(f'{cls.__name__} fed_convert_obj_to_xml:  The obj is undefined')
        if not fed_type_subtype or len(fed_type_subtype) > 2:
            raise Exception(f'{cls.__name__} fed_convert_obj_to_xml:  Format defined incorrectly')
        if not fed_version_subversion or len(fed_version_subversion) > 2:
            raise Exception(f'{cls.__name__} fed_convert_obj_to_xml: Version defined incorrectly')
        params = {
            'ТипДокумента': fed_type_subtype[0],
            'ПодТипДокумента': fed_type_subtype[1] if len(fed_type_subtype) > 1 else '',
            'ВерсияФормата': fed_version_subversion[0],
            'ПодВерсияФормата': fed_version_subversion[1] if len(fed_version_subversion) > 1 else ''
        }

        ep = HelperSbis.get_online_end_point()
        return ep.Интеграция.ФЭДСгенерировать(pattern, params, obj)

    # endregion

    @classmethod
    async def get_server_datetime(cls, context):
        ep = HelperSbis.get_online_end_point()
        saby_info = ep.СБИС.ИнформацияОВерсии(None)
        return saby_info['ВнешнийИнтерфейс']['ДатаВремяЗапроса']

    @classmethod
    async def extsyncdoc_write(cls, context, connection_uuid, extsyncdoc, objects):
        try:
            return ExtSyncDocBl.Write({
                'ConnectionId': connection_uuid,
                'ExtSyncDoc': extsyncdoc,
                'ExtSyncObj': objects
            })
        except Exception as err:
            raise ExtException(err, action="extsyncdoc_write")

    @classmethod
    async def extsyncdoc_prepare(cls, context, extsyncdoc_uuid, direction):
        try:
            doc = ExtSyncDoc().init(sync_doc_uuid=extsyncdoc_uuid)
            return doc.prepare()
        except Exception as err:
            raise ExtException(err, action="extsyncdoc_prepare")

    @classmethod
    async def extsyncdoc_execute(cls, context, extsyncdoc_uuid, direction):
        try:
            return ExtSyncDocBl.Execute({
                'SyncDocId': extsyncdoc_uuid,
                'Direction': direction
            })
        except Exception as err:
            raise ExtException(err, action="extsyncdoc_execute")

    @classmethod
    async def extsyncdoc_prepare_saby(cls, context, extsyncdoc_uuid):
        try:
            return Connector.Prepare('Saby', uuid.UUID(extsyncdoc_uuid))
        except Exception as err:
            raise ExtException(err, action="extsyncdoc_prepare_saby")

    @classmethod
    async def extsyncdoc_get_obj_for_execute(cls, context, extsyncdoc_uuid, direction):
        try:
            return ExtSyncDocBl.GetObjectsForExecute(uuid.UUID(extsyncdoc_uuid), direction)
        except Exception as err:
            raise ExtException(err, action="extsyncdoc_get_obj_for_execute")

    @classmethod
    async def api3_extsyncdoc_read(cls, context, extsyncdoc_uuid):
        try:
            return API3.ExtSyncDocRead({'SyncDocId': extsyncdoc_uuid})
        except Exception as err:
            raise ExtException(err, action="api3_extsyncdoc_read")

    @classmethod
    async def api3_get_sbis_objects(cls, context, _type, _id):
        ep = HelperSbis.get_online_end_point()
        return ep.API3.GetSbisObject(_type, _id)

    @classmethod
    async def mapping_obj_find_and_update(cls, context, _filter, _data):
        return MappingObject.FindAndUpdate(_filter, _data)

    @classmethod
    async def download_from_link(cls, context, link):
        _repeat_count = 0
        while _repeat_count < 3000:
            try:
                result = ExtRequests().get(link, {'X-SBISSessionID': HelperSbis.get_session_by_user()})
                if result is None:
                    _repeat_count += 1
                    continue
                break
            except Exception as e:
                if "00000000-0000-0000-0000-1aa0000f1002" in str(e):
                    _repeat_count += 1
                    time.sleep(0.3)
                else:
                    raise Exception('Request error')
        else:
            raise Exception('Repeat request error')
        return result

    @staticmethod
    def convert_to_sbis_datetime(date_time):
        return date_time.strftime("%d.%m.%Y %H.%M.%S")
