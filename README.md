
# blockly_executor_saby

В репозитории находятся блоки и алгоритмы, необходимые для   взаимодействия с сервисом IntegrationConfig и online'ом, при выполнении автоматических операций

## Дополнительная информация[:](https://online.sbis.ru/opendoc.html?guid=09d087bc-8201-485d-83d6-f605362a01b6)

- Ответственный: [Разговоров М. А.](https://online.sbis.ru/person/a6ccda3a-a3a5-4bcc-a76d-232bf3d305c8)

- [Техническая документация](https://online.sbis.ru/shared/disk/f3db3864-433e-47cd-b7bc-86efa801cb97)
