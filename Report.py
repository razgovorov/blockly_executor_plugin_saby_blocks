from uuid import uuid4
from blockly_executor import ExtException
from blockly_executor.core.helpers.data_getters_setter import data_getter_root, data_setter_root


class Report:
    def __init__(self, *, operation=None, uuid=None, service=None):
        self.operation = operation
        self.uuid = uuid
        self.service = service
        self.data = {}

    async def read(self):
        if not self.uuid:
            self.uuid = str(uuid4())
            if hasattr(self.operation, 'params'):
                self.params = self.operation.params
        else:
            self.data = await self.service.report_read(self.operation.uuid, self.uuid)

    async def update(self, context):
        self.blockly_context = context.to_dict()
        if self.params:
            try:
                await self.operation.update_params(self.params)
            except Exception as ex:
                context.operation['status'] = 'error'
                context.operation['data'] = f'Update operation params {ex}'
        try:
            self.service.report_update(self.operation.uuid, self.uuid, self.data)
        except ExtException as ex:
            context.operation['status'] = 'error'
            context.operation['data'] = f'Update report {ex.message} {ex.detail}'

    @property
    def params(self):
        return data_getter_root(self, 'params')

    @params.setter
    def params(self, value):
        data_setter_root(self, 'params', value)

    @property
    def blockly_context(self):
        return data_getter_root(self, 'blockly_context')

    @blockly_context.setter
    def blockly_context(self, value):
        data_setter_root(self, 'blockly_context', value)
