from blockly_executor.core.block_templates.simple_block import SimpleBlock


class ExtsysConnectorApi(SimpleBlock):
    suffix = 'find'
    calc_params = ['object']

    async def _calc_value(self, node, path, context, block_context):
        if self.command_sended(block_context):
            return self.command_get_result(block_context['__deferred'])
        else:

            ini_name = f"{block_context['ini_name']}_{self.suffix}"
            await update_ini(self.executor, context, ini_name)
            params = {
                'ini_name': ini_name
            }
            for elem in self.calc_params:
                params[elem] = block_context.get(elem)
            return self.command_send('calc_ini', params, context, block_context)


async def update_ini(executor, context, ini_name):
    try:
        cache = context.data['cache_ini']
    except KeyError:
        context.data['cache_ini'] = {}
        cache = context.data['cache_ini']
    if ini_name in cache:
        return
    if context.report is None:
        raise Exception('Report not defined')
    context.commands.append({
        'name': 'update_ini',
        'params': {
            'name': ini_name,
            'version': 0,
            'value': await context.report.service.workspace_read(context.report.operation, ini_name)
        }
    })
    cache[ini_name] = 0
