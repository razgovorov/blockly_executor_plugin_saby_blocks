from blockly_executor.core.block_templates.simple_block import SimpleBlock


class Api3Link(SimpleBlock):
    async def _calc_value(self, node, path, context, block_context):
        return {
            "ИдИС": block_context.get('ИдИС', ''),
            "ТипИС": block_context.get('ТипИС', ''),
            "ИмяИС": block_context.get('ИмяИС', ''),
            "ini_name": block_context.get('ini_name', '')
        }
