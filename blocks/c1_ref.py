from blockly_executor.core.block_templates.simple_block import SimpleBlock


class C1Ref(SimpleBlock):
    # эмуляция блока 1С для локальной отладки не надо нигде повторять
    async def _calc_value(self, node, path, context, block_context):
        return dict(
            type='c1ref',
            c1_type=block_context.get('TYPE', ''),
            c1_subtype=block_context.get('SUBTYPE', ''),
            c1_uuid=block_context.get('UUID', '')
        )
