from blockly_executor.plugins.saby.block_template.extsys_connector_api import ExtsysConnectorApi


class ExtsysFind(ExtsysConnectorApi):
    suffix = 'find'
    calc_params = ['object']


