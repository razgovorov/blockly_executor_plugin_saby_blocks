from blockly_executor.core.block_templates.multi_thread_loop import MultiThreadLoop


class SabyReadChanges(MultiThreadLoop):
    """
    RMA Блок чтения списка изменений с Online.
    Читаем список изменений по дате и/или идентификатору последнего события, сохраненным на коннекшене.
    Блок выполняется, пока не будет возвращен пустой список изменений.
    Параметры контекста на коннекшене context.report.params (узел json_connection):
        page_size      - количество событий, обрабатываемых в одной итерации. По умолчанию 25, максимальный 50.
        last_event: {  - данные последнего чтения списка изменений
            id         - Идентификатор последнего полученного события
            datetime   - дата/время последнего полученного события
        }
    """

    async def _get_items(self, context, block_context):
        """
        RMA Чтение списка изменений с Online
        VII Вынес формирование фильтра в блок, Рефакторинг
        :param context: Контекст с подключения context.report.params(Значение узла json_connection подключения ?)
        :type context: dict (?)
        :param block_context: Контекст операции (Значение узла Data соответствующего элемента таблицы ExtSyncDoc ?)
        :type block_context: dict (?)
        :return: Список изменившихся объектов
        :rtype: list
        """
        # Получем идентификато и дату от послденего запроса списка изменений
        last_event = context.report.params.get('last_event', {})
        last_event_id = last_event.get('id', None)

        last_event_datetime = last_event.get('date')
        if not last_event_datetime and not last_event_id:
            last_event_datetime = await context.report.service.get_server_datetime(context.report.params)
            context.report.params['last_event'] = {
                'date': last_event_datetime
            }

        # Формирование фильтра
        _filter = {
            'Навигация': {
                'РазмерСтраницы': str(context.report.params.get('page_size', 25))
            }
        }
        if last_event_datetime:
            _filter['ДатаВремяС'] = last_event_datetime
        if last_event_id:
            _filter['ИдентификаторСобытия'] = last_event_id
        try:
            events = await context.report.service.read_changes(context.report.params, _filter)
        except Exception as err:
            if 'Не найдено событие с идентификатором' in str(err):
                _filter.pop('ИдентификаторСобытия')
                events = await context.report.service.read_changes(context.report.params, _filter)
            else:
                raise Exception(str(err))
        events = events.get('Документ', [])
        return events

    async def _execute_item(self, node_loop, path, context, block_context):
        self.logger.debug(
            f'{self.__class__.__name__} {self.block_id} execute page {block_context["page"]} item {block_context["index"]}')

        event = block_context['items'][block_context['index']]['Событие'][0]
        context.report.params['last_event'] = {
            'id': event['Идентификатор'],
            'date': event['ДатаВремя']
        }

        await self.execute_all_next(node_loop, path, context, block_context, True)
        pass
