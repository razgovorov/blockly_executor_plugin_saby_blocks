from blockly_executor.plugins.saby.block_template.extsys_connector_api import ExtsysConnectorApi


class ExtsysQuery(ExtsysConnectorApi):
    suffix = 'query'
    calc_params = ['filter', 'limit', 'offset']
