""" Блок получения XML документа по подстановке """
__author__ = 'vlasov.ii'

from blockly_executor.core.block_templates.simple_block import SimpleBlock


class FedConvertObjectToXml(SimpleBlock):
    """
    Класс получения XML документа по подстановке
    VII Блок является мостом для вызова метода ФЭД.Сгенерировать(2). Параметрами его вызова являются параметры
    документа (ТипДокумента, ПодТипДокумента, ВерсияФормата, ПодВерсияФормата) и сама подстановка.
    На блоке указываются параметры:
        data    - Структура подстановки документа
        format  - Тип и ПодТип документа в формате "ТипДокумента_ПодТипДокумента"
        version - Верчия и ПодВерсия формата документа в формате "ВерсияФормата_ПодВерсияФормата"
        !!! На данный момент укзанноя логика, вожможно стоит разбить format и version на  4 параметра !!!
    """

    async def _calc_value(self, node, path, context, block_context):
        """
        Расчет значения блока.
        VII Получение XML документа по подстановке.
        :param node:
        :param path:
        :param context:
        :param block_context: Контекст выполнения блока с указанными и рассчитанными значениями.
        :return: XML Документа в base64
        :rtype: str
        """
        # obj = block_context.get('data')
        # _format = block_context.get('format', '').split('_')
        # _version = block_context.get('version', '').split('_')
        # if not obj:
        #     raise Exception(f'{self.__class__.__name__} {self.block_id} The obj is undefined')
        # if not _format or len(_format) != 2:
        #     raise Exception(f'{self.__class__.__name__} {self.block_id} Format defined incorrectly')
        # if not _version or len(_version) != 2:
        #     raise Exception(f'{self.__class__.__name__} {self.block_id} Version defined incorrectly')
        # params = {
        #     'ТипДокумента': _format[0],
        #     'ПодТипДокумента': _format[1],
        #     'ВерсияФормата': _version[0],
        #     'ПодВерсияФормата': _version[1]
        # }
        #
        # if not params:
        #     raise Exception(f'{self.__class__.__name__} {self.block_id} Document parameters is undefined')

        result = await context.report.service.fed_convert_obj_to_xml(
            context.report.params,
            block_context.get('data'),
            block_context.get('format'),
            block_context.get('version'),
            block_context.get('pattern')
        )
        return result
