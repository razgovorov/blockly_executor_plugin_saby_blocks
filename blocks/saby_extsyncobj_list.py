from blockly_executor.core.block_templates.multi_thread_loop import MultiThreadLoop


class SabyExtsyncobjList(MultiThreadLoop):
    """
    RMA Блок чтения ExtSyncObject с Online.
    Читаем список объекьтов по идентификатору extsyncdoc.
    """

    async def _get_items(self, context, block_context):
        extsyncdoc_uuid = context.operation['operation_uuid']
        extra_fields = []

        filter = {}

        sorting = []
        pagination = {
            'PageSize': 50,
            'Page': block_context.get('page', 0)

        }
        result = await context.report.service.extsyncobj_list(
            context.report.params, extsyncdoc_uuid, extra_fields, filter, sorting, pagination)
        return result.get('Result', [])
