from blockly_executor.core.block_templates.simple_block import SimpleBlock
from blockly_executor.core.exceptions import LimitCommand, StepForward, DeferredOperation
from blockly_executor import ExtException
from blockly_executor.plugins.saby.block_template.extsys_connector_api import update_ini
from uuid import uuid4


class ExtsyncdocRun(SimpleBlock):
    """
    """

    async def _calc_value(self, node, path, context, block_context):
        """
        """
        block_context['prepare_counter'] = block_context.get('prepare_counter', 0)
        block_context['__deferred'] = block_context.get('__deferred', {})

        try:
            await self.process_commands_result(context, block_context)
            await self.extsyncdoc_prepare_is(context, block_context)
            await self.extsyncdoc_prepare_saby(context, block_context)
            await self.extsyncdoc_execute_saby(context, block_context)
            await self.extsyncdoc_execute_is(context, block_context)
        except DeferredOperation as err:
            raise err from err
        except StepForward as err:
            raise err from err
        except LimitCommand as err:
            raise err from err
        except Exception as err:
            self.set_variable(context, '_last_error', str(err))
            return False

        result = await self.extsyncdoc_read(context, block_context)
        return result

    async def command_syncdocfill(self, context, block_context, ext_sync_obj):
        raise NotImplemented()

    async def command_processpredefineobject(self, context, block_context, ext_sync_obj):
        raise NotImplemented()

    async def command_getobject(self, context, block_context, ext_sync_obj):
        obj = ext_sync_obj.get('Context', {})
        obj['ИдИС'] = ext_sync_obj['ClientId']
        await self.add_command_calc_ini(context, block_context, 'read', ext_sync_obj, obj)

    async def command_update(self, context, block_context, ext_sync_obj):
        try:
            obj = ext_sync_obj['Data']['data']
        except KeyError as key:
            raise Exception(f'Объект не содержит необходимый параметр {key}')
        await self.add_command_calc_ini(context, block_context, 'update', ext_sync_obj, obj)

    async def command_find(self, context, block_context, ext_sync_obj):
        try:
            obj = ext_sync_obj['Data']['data']
        except KeyError as key:
            raise Exception(f'Объект не содержит необходимый параметр {key}')
        await self.add_command_calc_ini(context, block_context, 'find', ext_sync_obj, obj)

    async def add_command_calc_ini(self, context, block_context, command, ext_sync_obj, obj):
        obj_name = ext_sync_obj['Data']['ini_name'][13:]
        ini_name = f'{obj_name}_{command}'
        await update_ini(self.executor, context, ini_name)
        command_uuid = str(uuid4())
        block_context['__deferred'][command_uuid] = ext_sync_obj
        block_context['__deferred'][command_uuid]['_command'] = command
        self.executor.commands.append(
            {
                'name': 'calc_ini',
                'params': {
                    'ini_name': ini_name,
                    'object': obj
                },
                'uuid': command_uuid
            }
        )

    async def process_commands_result(self, context, block_context):
        connection_uuid = context.operation['connection_uuid']
        extsyncdoc_uuid = context.operation['operation_uuid']
        objects = []
        if block_context['__deferred']:  # ждем команды
            commands = list(block_context['__deferred'])
            for command_uuid in commands:
                command = block_context['__deferred'].pop(command_uuid)
                try:
                    result = self.command_get_result(command_uuid)
                    await getattr(self, f'process_command_result_{command["_command"]}')(context, block_context,
                                                                                         command, result, objects)
                except ExtException as err:
                    _data = command[1]
                    _data['StatusId'] = 'Ошибка'
                    _data['StatusMsg'] = [err.message]
                    _data['Data']['error'] = err.to_dict()
                    objects.append(_data)
                except Exception as err:
                    _data = command
                    _data['StatusId'] = 'Ошибка'
                    _data['StatusMsg'] = [str(err)]
                    objects.append(_data)

            try:
                await context.report.service.extsyncdoc_write(
                    context.report.params,
                    connection_uuid,
                    {
                        'Uuid': extsyncdoc_uuid,
                    },
                    objects
                )
            except Exception as err:
                self.set_variable(context, '_last_error', str(err))
                return False

            if block_context['__deferred']:  # добавились новые команды
                raise LimitCommand()

    async def process_command_result_read(self, context, block_context, command, result, objects):
        if 'ИдИС' in result:
            objects.append({
                'ClientId': result['ИдИС'],
                'Id': result['ИдИС'],
                'Type': f"{result['ТипИС']}.{result['ИмяИС']}",
                'StatusId': 'Получен',
                'Action': 1,
                'Data': {
                    'ini_name': command['Data']['ini_name'],
                    'data_is': result,
                    'subobject': command['Data']['subobject'],
                    'name': result.get('Название')
                }
            })
        else:
            _data = command
            _data['Data'] = {
                'ini_name': command['Data']['ini_name'],
                'raw_data': result,
                'subobject': command['Data'].get('subobject', False)
            }
            objects.append(_data)

    async def process_command_result_find(self, context, block_context, command, result, objects):
        if 'ИдИС' in result:
            command['Data']['data']['ИдИС'] = result['ИдИС']
        await self.command_update(context, block_context, command)

    async def process_command_result_update(self, context, block_context, command, result, objects):
        connection_uuid = context.operation['connection_uuid']

        if 'ИдИС' not in command['Data']['data'] and 'ИдИС' in result:  # создаем объект в ИС
            # записываем идентификатор в маппинг
            _filter = {
                'ConnectionId': connection_uuid,
                'Type': command['Data']['data']['ИмяСБИС'],
                'Id': command['Data']['data']['ИдСБИС'],
                'IdType': 1
            }
            _data = {
                'ClientId': result['ИдИС'],
                'ClientName': result['Название']
            }
            context.report.service.mapping_obj_find_and_update(
                context.report.params,
                _filter,
                _data
            )
            pass
        objects.append({
            'Uuid': command['Uuid'],
            'StatusId': 'Синхронизирован'
        })

    async def extsyncdoc_prepare_is(self, context, block_context):
        extsyncdoc_uuid = context.operation['operation_uuid']
        connection_uuid = context.operation['connection_uuid']
        while True:
            block_context['prepare_counter'] += 1
            if block_context['prepare_counter'] >= 3000:
                raise Exception('Превышено количество циклов prepare для операции экспорта')
            try:
                result = await context.report.service.extsyncdoc_prepare(context.report.params, extsyncdoc_uuid, 1)
            except Exception as err:
                raise err from err

            for key in result:
                block_context[key] = result[key]
            actions = block_context.get('requiredActions', [])
            count_actions = len(actions)
            if count_actions:
                self.logger.debug(f"ExtSyncDoc.prepare actions: {len(result['requiredActions'])}")
                errors = []  # ошибки запуска команд
                for command in result['requiredActions']:
                    try:
                        await getattr(self, f'command_{command[0].lower()}')(context, block_context, command[1])
                    except ExtException as err:
                        _data = command[1]
                        _data['StatusId'] = 'Ошибка'
                        _data['StatusMsg'] = [err.message]
                        _data['Data']['error'] = err.to_dict()
                        errors.append(_data)
                    except Exception as err:
                        _data = command[1]
                        _data['StatusId'] = 'Ошибка'
                        _data['StatusMsg'] = [str(err)]
                        errors.append(_data)

                if errors:
                    res = await context.report.service.extsyncdoc_write(
                        context.report.params,
                        connection_uuid,
                        {
                            'Uuid': extsyncdoc_uuid,
                        },
                        errors
                    )

                raise DeferredOperation()

            if not count_actions and block_context['all_objects'] <= block_context['count_processed'] + \
                    block_context['count_error']:
                if block_context['count_error']:
                    raise Exception('Ошибки подготовки')
                break

    async def extsyncdoc_prepare_saby(self, context, block_context):
        extsyncdoc_uuid = context.operation['operation_uuid']
        await context.report.service.extsyncdoc_prepare_saby(context.report.params, extsyncdoc_uuid)

    async def extsyncdoc_execute_saby(self, context, block_context):
        extsyncdoc_uuid = context.operation['operation_uuid']
        result = await context.report.service.extsyncdoc_execute(context.report.params, extsyncdoc_uuid, 1)
        return

    async def extsyncdoc_execute_is(self, context, block_context):
        # connection_uuid = context.operation['connection_uuid']
        extsyncdoc_uuid = context.operation['operation_uuid']

        while True:
            block_context['prepare_counter'] += 1
            if block_context['prepare_counter'] >= 3000:
                raise Exception('Превышено количество циклов prepare для операции')
            try:
                result = await context.report.service.extsyncdoc_get_obj_for_execute(context.report.params, extsyncdoc_uuid, 2)
            except Exception as err:
                raise err from err
            if not result:
                return True
            for obj in result:
                if 'ИдИС' not in obj:
                    await self.command_find(context, block_context, obj)
                else:
                    await self.command_update(context, block_context, obj)
            raise LimitCommand()

    async def extsyncdoc_read(self, context, block_context):
        extsyncdoc_uuid = context.operation['operation_uuid']
        result = await context.report.service.api3_extsyncdoc_read(context.report.params, extsyncdoc_uuid)
        return result
