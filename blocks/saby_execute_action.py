from blockly_executor.core.block_templates.simple_block import SimpleBlock
import json


class SabyExecuteAction(SimpleBlock):

    async def _calc_value(self, node, path, context, block_context):
        """
        Расчет значения блока.
        VII Расчёт различий полученных объектов в соответствии с логикой в описании класса.
        :param node:
        :param path:
        :param context:
        :param block_context: Контекст выполнения блока с указанными и рассчитанными значениями.

        :return:
        :rtype: dict
        """

        сheck_result = self.check_props(node, path, context, block_context)
        if not сheck_result:
            return False
        doc = block_context['DOCUMENT']
        easy_send = self._get_mutation(node, 'EASY_SEND')
        if easy_send == 'TRUE':
            action_name = 'ПростоОтправить'
        else:
            action_name = block_context['ACTION']
        comment = block_context.get('COMMENT', '')

        stage = doc['Этап'][0] if isinstance(doc['Этап'], list) else doc['Этап']
        stage['Действие'] = {
            'Название': action_name,
            'Комментарий': comment
        }
        if easy_send == 'TRUE':
            stage['Действие']['Название'] = 'ПростоОтправить'
            stage['Название'] = 'ПростаяОтправка'
        attachments = self.get_attachment(node, path, context, block_context)
        if attachments:
            doc1 = {
                'Идентификатор': doc['Идентификатор'],
                'Этап': stage,
                'Вложение': attachments
            }
            try:
                await context.report.service.write_attachment(context.report.params, doc1)
            except Exception as err:
                self.set_variable(context, '_last_error', f'СБИС.ЗаписатьВложение {err}')
                self.logger.error(f'СБИС.ЗаписатьВложение {err}')
                self.logger.error(json.dumps(doc1, ensure_ascii=False))
                return False

        doc1 = {
            'Идентификатор': doc['Идентификатор'],
            'Этап': stage,
        }
        try:
            prepare_result = await context.report.service.prepare_action(context.report.params, doc1)
        except Exception as err:
            context.variables['_last_error'] = str(err)
            self.set_variable(context, '_last_error', f'СБИС.ПодготовитьДействие {err}')
            self.logger.error(f'СБИС.ПодготовитьДействие {err}')
            self.logger.error(json.dumps(doc1, ensure_ascii=False))
            return False

        sign_array = None  # await self.get_signature(context, doc1, prepare_result)

        try:
            prepared_stage = prepare_result.get('Этап')
            execute_action = {'Идентификатор': prepare_result.get('Идентификатор')}
            if not prepared_stage:
                raise Exception(f'{self.__class__.__name__} {self.block_id} Failed to prepare the document stage')
            if isinstance(prepared_stage, list):
                execute_action['Этап'] = prepared_stage[0]
                if sign_array:
                    for i, attach in enumerate(execute_action['Этап']['Вложение']):
                        attach['Подпись'] = [{'Файл': {'ДвоичныеДанные': sign_array[i]}}]
            return await context.report.service.execute_action(context.report.params, execute_action)
        except Exception as err:
            context.variables['_last_error'] = str(err)
            self.set_variable(context, '_last_error', f'СБИС.ВыполнитьДействие {err}')
            self.logger.error(f'СБИС.ВыполнитьДействие {err}')
            self.logger.error(json.dumps(prepare_result, ensure_ascii=False))
            return False

    def check_props(self, node, path, context, block_context):
        try:

            doc = block_context['DOCUMENT']
            easy_send = self._get_mutation(node, 'EASY_SEND')
            if easy_send == 'TRUE':
                action_name = 'ПростоОтправить'
            else:
                action_name = block_context['ACTION']
        except KeyError as key:
            context.variables['_last_error'] = f'{self.__class__.__name__} {self.block_id}  не заполнен обязательный параметр {key}'
            return False
        if not doc or not isinstance(doc, dict):
            context.variables['_last_error'] = f'{self.__class__.__name__} {self.block_id} Param Document defined incorrectly'
            return False
        if not action_name or not isinstance(action_name, str):
            context.variables['_last_error'] = f'{self.__class__.__name__} {self.block_id} Param Action name defined incorrectly'
            return False
        if not doc.get('Идентификатор') or not isinstance(doc['Идентификатор'], str):
            context.variables['_last_error'] = f'{self.__class__.__name__} {self.block_id} Param Document defined incorrectly'
            return False

        stage = doc.get('Этап')
        if not isinstance(stage, list):
            context.variables['_last_error'] = f'{self.__class__.__name__} {self.block_id} Param Document defined incorrectly'
            return False
        if not stage:
            context.variables['_last_error'] = f'{self.__class__.__name__} {self.block_id} Document dont have specified stage'
            return False
        return True

    def get_attachment(self, node, path, context, block_context):
        attachments = []
        attachment_types = self._get_mutation(node, 'attachment_types')
        if attachment_types:
            attachment_types = attachment_types.split(',')
        for att_index, att_type in enumerate(attachment_types):
            if att_type == 'att_array':
                try:
                    attachment_array = block_context[f'ATT{att_index}']
                except KeyError:
                    raise Exception(f'{self.__class__.__name__} не указаны данные вложения')
                attachments = attachments + attachment_array
            elif att_type == 'att_b64':
                try:
                    attachment_data = block_context[f'ATT{att_index}_DATA']
                except KeyError:
                    raise Exception(f'{self.__class__.__name__} не указаны данные вложения')
                attachment_title = block_context.get(f'ATT{att_index}_TITLE')
                attachment = {
                    "Файл": {
                        "Имя": attachment_title,
                        "ДвоичныеДанные": attachment_data
                    }
                }
                attachments.append(attachment)
            else:
                raise Exception(f'{self.__class__.__name__} неподдерживаемы тип вложения {att_type}')
        return attachments

    async def get_signature(self, context, doc1, prepare_result):
        for stage in prepare_result['Этап']:
            for action in stage['Действие']:
                if action['ТребуетПодписания'] == 'Да' and action.get('Сертификат'):
                    try:
                        sign_array = []
                        for sert in action['Сертификат']:
                            sign_dict = {'signature': sert['Отпечаток'], 'file': []}
                            for attach in stage['Вложение']:
                                sign_dict['file'].append({'Ссылка', attach['Файл']['Ссылка']})
                            sign_array.append(sign_dict)
                        operation_uuid = await context.report.service.init_remote_signing(context.report.params, sign_array)
                        try:
                            result = await context.report.service.get_remote_signature(context.report.params, operation_uuid)
                            return result
                        except Exception as err:
                            context.variables['_last_error'] = str(err)
                            self.set_variable(context, '_last_error', f'API3.GetRemoteSignature {err}')
                            self.logger.error(f'API3.RemoteSigning {err}')
                            self.logger.error(json.dumps(doc1, ensure_ascii=False))
                    except Exception as err:
                        context.variables['_last_error'] = str(err)
                        self.set_variable(context, '_last_error', f'API3.InitRemoteSigning {err}')
                        self.logger.error(f'API3.RemoteSigning {err}')
                        self.logger.error(json.dumps(doc1, ensure_ascii=False))
