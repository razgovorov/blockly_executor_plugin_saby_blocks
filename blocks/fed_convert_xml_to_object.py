from blockly_executor.core.block_templates.simple_block import SimpleBlock


class FedConvertXmlToObject(SimpleBlock):

    async def _calc_value(self, node, path, context, block_context):
        result = await context.report.service.fed_convert_xml_to_obj(
            context.report.params,
            block_context.get('pattern'),
            block_context.get('data')
        )
        return result
