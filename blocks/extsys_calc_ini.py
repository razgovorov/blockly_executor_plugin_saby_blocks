from blockly_executor import ExtException
from blockly_executor.core.exceptions import LimitCommand, StepForward, DeferredOperation, ReturnFromFunction
from blockly_executor.plugins.saby.block_template.extsys_connector_api import ExtsysConnectorApi
from blockly_executor.plugins.saby.block_template.extsys_connector_api import update_ini


class ExtsysCalcIni(ExtsysConnectorApi):

    async def _calc_mutation(self, node, path, context, block_context, mutation_number):
        node_key = node.find(f"./b:field[@name='KEY{mutation_number}']", self.ns)
        key = node_key.text
        node_value = node.find(f"./b:value[@name='VALUE{mutation_number}']", self.ns)
        if key and node_value:
            result = await self.execute_all_next(node_value, f'{path}.param{mutation_number}', context, block_context)
            return {'name': key, 'value': result}
        return None

    async def _calc_value(self, node, path, context, block_context):
        if self.command_sended(block_context):
            try:
                return self.command_get_result(block_context['__deferred'])
            except Exception as err:
                raise ExtException(
                    parent=err,
                    dump={
                        'block_context': block_context,
                        'block_id': self.block_id,
                        'full_name': self.full_name
                    }
                )
        else:
            try:
                params_count = int(self._get_mutation(node, 'items', 0))
                ini_name = block_context['INI_NAME']
                params = {
                    'ini_name': ini_name,
                }
                for i in range(params_count):
                    key = block_context.get(f'PARAM{i}_NAME')
                    value = block_context.get(f'PARAM{i}_VALUE')
                    params[key] = value
                await update_ini(self.executor, context, ini_name)
                self.command_send('calc_ini', params, context, block_context)
            except (LimitCommand, StepForward, ReturnFromFunction, DeferredOperation) as err:
                raise err from err
            except Exception as err:
                raise ExtException(parent=err, action=f'{self.__class__.__name__}') from err
