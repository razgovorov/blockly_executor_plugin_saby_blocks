from blockly_executor.core.block_templates.simple_block import SimpleBlock


class SabyWriteDocument(SimpleBlock):
    """
    RMA Блок чтения документа с Online.
    Читаем список изменений по дате и/или идентификатору последнего события, сохраненным на коннекшене.
    Блок выполняется, пока не будет возвращен пустой список изменений.
    Параметры контекста на операции (узел Data соответствующего элемента таблицы ExtSyncDoc):
        doc - Информация о документе который необходимо прочитать. Допускаются два варианта:
            1. doc - стока с идентификатором документа
            2. doc - словарь с идентификатором документа в поле 'Идентификатор'
    """

    async def _calc_value(self, node, path, context, block_context):
        """
        RMA Чтение документа с Online
        VII Рефакторинг
        :param node:
        :type node:
        :param path:
        :type path:
        :param context: Контекст с подключения context.report.params(Значение узла json_connection подключения ?)
        :type context: dict (?)
        :param block_context: Контекст операции (Значение узла Data соответствующего элемента таблицы ExtSyncDoc ?)
        :type block_context: dict (?)
        :return: Список изменившихся объектов
        :rtype: list
        """
        try:
            doc = block_context['doc']
            if not (isinstance(doc, dict) and doc.get('Идентификатор')):
                raise Exception('not supported document type')
            return await context.report.service.write_document(context.report.params, doc)
        except KeyError:
            raise Exception(f'{self.__class__.__name__} {self.block_id} params not defined')
        except Exception as err:
            context.variables['_last_error'] = str(err)
            return False
