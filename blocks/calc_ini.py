from blockly_executor.core.exceptions import StepForward, LimitCommand, DeferredOperation, ReturnFromFunction
from blockly_executor.core.block_templates.simple_block import SimpleBlock
from blockly_executor.plugins.saby.block_template.extsys_connector_api import update_ini


class CalcIni(SimpleBlock):

    # async def _calc_value(self, node, path, context, block_context):
    #     ini_name = f"{block_context['ini_name']}_{self.suffix}"
    #     await update_ini(self.executor, context, ini_name)
    #     params = {
    #         'ini_name': ini_name
    #     }
    #     for elem in self.calc_params:
    #         params[elem] = block_context.get(elem)
    #     return self.command_send('calc_ini', params, context, block_context)
    #
    # async def _calc_mutation(self, node, path, context, block_context, mutation_number):
    #     node_key = node.find(f"./b:field[@name='KEY{mutation_number}']", self.ns)
    #     key = node_key.text
    #     node_value = node.find(f"./b:value[@name='VALUE{mutation_number}']", self.ns)
    #     if key and node_value:
    #         result = await self.execute_all_next(node_value, f'{path}.param{mutation_number}', context, block_context)
    #         return {'name': key, 'value': result}
    #     return None

    async def _calc_value(self, node, path, context, block_context):
        params_count = int(self._get_mutation(node, 'items', 0))
        ini_name = block_context['INI_NAME']
        endpoint = block_context.get('ENDPOINT', 'main')
        workspace_xml = await context.report.service.workspace_read(context.report.operation, ini_name)
        current_algorithm = self.executor.algorithm
        if '_child_variables' not in block_context:
            block_context['_child_variables'] = {}
            for i in range(params_count):
                key = block_context.get(f'PARAM{i}_NAME')
                value = block_context.get(f'PARAM{i}_VALUE')
                block_context['_child_variables'][key] = value

        context = context.init_nested(block_context)
        result = await self.executor.execute_nested(
            workspace_xml,
            context,
            endpoint=endpoint,
            # commands_result=self.executor.commands_result,
            algorithm=ini_name
        )
        self.executor.algorithm = current_algorithm
        return result
