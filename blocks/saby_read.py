from blockly_executor.core.block_templates.simple_block import SimpleBlock


class SabyRead(SimpleBlock):
    """

    """

    async def _calc_value(self, node, path, context, block_context):
        """
        """
        try:
            _type = block_context['type']
            _id = block_context['id']
            single = False
        except KeyError:
            raise Exception(f'{self.__class__.__name__} {self.block_id} params not defined')

        if isinstance(_id, str):
            _id = [_id]
            single = True

        result = await context.report.service.api3_get_sbis_objects(context.report.params, _type, _id)
        if single:
            return result[0]
        return result

