from blockly_executor.core.block_templates.simple_block import SimpleBlock
import base64


class SabyDownloadFromLink(SimpleBlock):

    async def _calc_value(self, node, path, context, block_context):
        try:
            res = await context.report.service.download_from_link(context.report.params, block_context['link'])
            res = base64.b64encode(res).decode(encoding='utf-8')
            return res

        except KeyError as err:
            raise KeyError(f'{self.__class__.__name__} {err} not defined')
