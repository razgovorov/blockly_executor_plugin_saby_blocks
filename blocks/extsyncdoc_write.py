from blockly_executor.core.block_templates.simple_block import SimpleBlock
# from blockly_executor.plugins.saby.block_template.extsys_connector_api import update_ini


class ExtsyncdocWrite(SimpleBlock):
    """
    """

    async def _calc_value(self, node, path, context, block_context):
        """
        """
        try:
            direction = block_context['direction']
            objects = block_context['objects']
        except KeyError:
            raise Exception(f'{self.__class__.__name__} {self.block_id} params not defined')

        if direction == 'import':
            direction = 1
            objects = await self.prepare_ext_sync_objects(context, direction, objects)
        elif direction == 'export':
            direction = 2
            objects = await self.prepare_ext_sync_objects(context, direction, objects)
        elif direction == 'import_api3_obj':
            direction = 1
            objects = await self.prepare_api3_objects(context, direction, objects)
        elif direction == 'import_api3_link':
            direction = 1
            objects = await self.prepare_api3_link(context, direction, objects)
        else:
            raise Exception('Хрень какая то')
        extsyncdoc_uuid = context.operation['operation_uuid']
        connection_uuid = context.operation['connection_uuid']
        try:
            await context.report.service.extsyncdoc_write(
                context.report.params,
                connection_uuid,
                {
                    'Uuid': extsyncdoc_uuid,
                    'Direction': direction
                },
                objects
            )
            return True
        except Exception as err:
            self.set_variable(context, '_last_error', str(err))
            return False

    async def prepare_ext_sync_objects(self, context, direction, objects):
        for obj in objects:
            obj['Action'] = direction
        return objects

    async def prepare_api3_link(self, context, direction, objects):
        _objects = []
        for obj in objects:
            _id = obj['ИдИС']
            ini_name = obj['ini_name']
            # await update_ini(self.executor, context, ini_name)
            _object = {
                'Id': _id,
                'Action': direction,
                'Title': obj.get('Название', _id),
                'Type': f"{obj['ТипИС']}.{obj['ИмяИС']}",
                'ClientId': _id,
                'Data': {
                    'data_is': obj,
                    'ini_name': ini_name,
                },
                'StatusId': None
            }

            _objects.append(_object)

        return _objects

    async def prepare_api3_objects(self, context, direction, objects):
        _objects = []
        for obj in objects:
            _id = obj['ИдИС']
            ini_name = obj.get('ini_name')
            _object = {
                'Id': _id,
                'Action': direction,
                'Title': obj.get('Название', _id),
                'Type': f"{obj['ТипИС']}.{obj['ИмяИС']}",
                'ClientId': _id,
                'Data': {
                    'data_is': obj,
                    'ini_name': ini_name,
                },
                'StatusId': 'Получен',
            }
            # if not _ini_name:
            #     # update_ini
            _objects.append(_object)

        return _objects
