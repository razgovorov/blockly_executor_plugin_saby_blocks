""" Блок сравнения документов """
__author__ = 'ii.vlasov'

import xml.etree.ElementTree as ET
from base64 import b64encode
from datetime import datetime

from blockly_executor import Helper
from blockly_executor.core.block_templates.simple_block import SimpleBlock
from blockly_executor.core.helpers.compare_objects import compare, index_list, get_compare_object


class SabyCreateActOfDivergence(SimpleBlock):
    """
    VII Блок для создания акта росхождения. На вход объект с данными документа, и объект с данными факта.
    Возвращает base64  строки с xml акта расхождения.
    """

    async def _calc_value(self, node, path, context, block_context):
        accounting = block_context.get('accounting')
        actually = block_context.get('actually')
        result = self.create_act_of_divergence(accounting, actually)
        return result

    def create_act_of_divergence(self, accounting, actually, difference_param=None):
        xml = b''
        obj1 = accounting
        obj2 = actually
        if difference_param:
            block_context = difference_param
        else:
            block_context = {
                'OBJECT1': obj1,
                'OBJECT2': obj2,
                'PROPS': [],
                'TABLE0_NAME': 'ТабличныеДанные',
                'TABLE0_GROUP': ['НомНомер'],
                'TABLE0_FIELDS': ['Количество',
                                  'СуммаЦенБезНДС',
                                  'Цена',
                                  'СуммаНДС',
                                  'ЕдИзм',
                                  'Наименование',
                                  'ЕдКод',
                                  'НомНомер',
                                  'ЦенаБезНДС']
            }

        difference = False
        delta = {}

        def difference_string(_delta, _prefix=None):
            result = ''
            if _prefix:
                result += f'{_prefix}: '
            for key in _delta:
                result += f'{key}, '
            return f'{result[:-2]}; '

        obj1 = block_context.get('OBJECT1')
        obj2 = block_context.get('OBJECT2')
        props = block_context.get('PROPS')
        delimiter = '.'
        result_difference = ''
        if props:
            obj1_props = get_compare_object(obj1, props, delimiter=delimiter)
            obj2_props = get_compare_object(obj2, props, delimiter=delimiter)
            difference, delta = compare(obj1_props, obj2_props)
            if difference:
                result_difference += difference_string(delta)

        compare_table_index = 0
        group = None
        obj1_props = None
        obj2_props = None
        while True:
            compare_table_path = block_context.get(f'TABLE{compare_table_index}_NAME')
            if not compare_table_path:
                break
            group = block_context.get(f'TABLE{compare_table_index}_GROUP')
            fields = block_context.get(f'TABLE{compare_table_index}_FIELDS')
            table1 = Helper.obj_get_path_value(obj1, compare_table_path)
            table2 = Helper.obj_get_path_value(obj2, compare_table_path)
            obj1_props = index_list(table1, group, fields, delimiter=delimiter)
            obj2_props = index_list(table2, group, fields, delimiter=delimiter)
            difference, delta = compare(obj1_props, obj2_props)
            compare_table_index += 1
        if difference:
            xml = self.create_act_of_divergence_3_03(delta, accounting, group, obj1_props, obj2_props)
        return b64encode(xml).decode("utf-8")

    def create_act_of_divergence_3_03(self, delta, accounting, group, table1, table2):
        root = ET.Element("Файл", Формат="АктРасхождение", ВерсияФормата="3.03", Имя="SE_TORG2_")
        document = ET.Element("Документ", Дата=datetime.now().strftime('%d.%m.%Y'), Номер=accounting.get("Номер"),
                              Название="Акт о расхождении")
        root.append(document)
        parties = ET.Element("Стороны")
        document.append(parties)
        res = self.search_by_mask("Отправитель.", accounting)
        if res:
            self.add_parties(parties, res, "Поставщик")
        res = self.search_by_mask("Получатель.", accounting)
        if res:
            self.add_parties(parties, res, "Покупатель")
        res = self.search_by_mask("Грузоотправитель.", accounting)
        if res:
            self.add_parties(parties, res, "Грузоотправитель")
        res = self.search_by_mask("Грузополучатель.", accounting)
        if res:
            self.add_parties(parties, res, "Грузополучатель")
        list_base = ET.Element("СписокОснование")
        count_base = 0
        for _node in accounting.get("Основание"):
            if _node.get("Дата") and _node.get("Номер"):
                base = ET.Element("Основание", Дата=_node.get("Дата"), Номер=_node.get("Номер"))
                list_base.append(base)
                count_base = count_base + 1
        if count_base > 0:
            document.append(list_base)
        list_delta = ET.Element("СписокСтрОтклонения")
        document.append(list_delta)
        for _node in delta:
            self.get_divergence_strings(list_delta, _node, table1, table2)
        return ET.tostring(root, encoding="windows-1251", method="xml")

    @staticmethod
    def search_by_mask(param, node):
        res = {}
        for _node in node:
            if _node.find(param) > -1:
                node_name = _node.replace(param, "")
                res[node_name] = node[_node]
        return res

    def get_address(self, node, res):
        address1 = self.search_by_mask("АдрРФ.", res)
        if address1:
            address_xml = ET.Element("АдрРФ")
            for _node in address1:
                address_xml.set(_node.replace("Адр", ""), address1.get(_node, ""))
            node.append(address_xml)

        address2 = self.search_by_mask("АдрИно.", res)
        if address2:
            address_xml = ET.Element("АдрИно")
            for _node in address2:
                address_xml.set(_node, address2.get(_node, ""))
            node.append(address_xml)

    def get_req(self, node, res):
        req1 = self.search_by_mask("СвЮл.", res)
        if req1.get("Название") and req1.get("ИНН") and req1.get("КПП"):
            ul = ET.Element("СвЮЛ", Название=req1["Название"], ИНН=req1["ИНН"], КПП=req1["КПП"])
            node.append(ul)
        req2 = self.search_by_mask("СвФл.", res)
        if req2.get("Название") and req1.get("ИНН"):
            fl = ET.Element("СвФЛ", Название=req1["Название"], ИНН=req1["ИНН"])
            node.append(fl)

    def add_parties(self, node, res, param):
        party = ET.Element(param)
        list_address = ET.Element("СписокАдрес")
        party.append(list_address)
        address = ET.Element("Адрес")
        list_address.append(address)
        self.get_address(address, self.search_by_mask("Адрес.", res))
        if res.get("Название"):
            party.set("Название", res["Название"])
        else:
            req = self.search_by_mask("СвЮл.", res)
            if req.get("Название"):
                party.set("Название", req["Название"])
            req = self.search_by_mask("СвФл.", res)
            if req.get("Название"):
                party.set("Название", req["Название"])
        node.append(party)
        self.get_req(party, res)

    @staticmethod
    def get_divergence_strings(list_delta, _node, table1, table2):
        def to_float(_float):
            try:
                res = float(_float)
            except:
                res = 0
            return res

        res_tab1 = table1[_node] if _node in table1 else {}
        res_tab2 = table2[_node] if _node in table2 else {}

        req_tab = res_tab1 if res_tab1 else res_tab2
        if not res_tab1:
            res_tab1 = res_tab2
        if not res_tab2:
            res_tab2 = res_tab1

        doc_string = ET.Element("СтрОтклонения")
        if req_tab.get("ЕдИзм") is not None:
            doc_string.set("ЕдИзм", req_tab["ЕдИзм"])
        if req_tab.get("Наименование") is not None:
            doc_string.set("Название", req_tab["Наименование"])
        if req_tab.get("ЕдКод") is not None:
            doc_string.set("ОКЕИ", req_tab["ЕдКод"])
        list_delta.append(doc_string)
        doc_string.set("Кол_во", str(
            to_float(res_tab2.get('Количество')) - to_float(res_tab1.get('Количество'))))
        doc_string.set("Сумма", str(
            to_float(res_tab2.get("СуммаЦенБезНДС")) - to_float(res_tab1.get("СуммаЦенБезНДС"))))
        in_doc = ET.Element("ПоДокументам",
                            Цена=str(to_float(res_tab1.get("ЦенаБезНДС"))),
                            Кол_во=str(to_float(res_tab1.get("Количество"))),
                            Сумма=str(to_float(res_tab1.get("СуммаЦенБезНДС"))))
        doc_string.append(in_doc)

        in_fact = ET.Element("ПоФакту",
                             Цена=str(to_float(res_tab2.get("ЦенаБезНДС"))),
                             Кол_во=str(to_float(res_tab2.get("Количество"))),
                             Сумма=str(to_float(res_tab2.get("СуммаЦенБезНДС"))))
        doc_string.append(in_fact)
