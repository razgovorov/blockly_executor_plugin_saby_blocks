from blockly_executor.core.block_templates.multi_thread_loop import MultiThreadLoop


class SabyTaskList(MultiThreadLoop):
    """
    RMA Блок чтения списка с Online.
    По умолчанию читаем все задачи "На мне"
    Гипотетически в закомментированном блоке в качестве параметров могут быть получены:
        1. Тип задач (значение поля фильтра): На мне ('Мои'), Разобрать ('Нераспределенные')
        2. Даты с и по которым требуется отобрать задачи в формате ГГГГ-ММ-ДД
        3. Маска для отбора задач (вхождение строки в текст задачи или номер)
    Блок выполняется, пока не будет возвращен пустой список задач.
    Параметры контекста на коннекшене context.report.params (узел json_connection):
        page_size - количество события, обрабатываемых в данной итерации. По умолчанию 25, максимальный 50.
    Параметры контекста на операции (узел Data соответствующего элемента таблицы ExtSyncDoc):
        page - Текущая обрабатываемая страница
    """

    async def _get_items(self, context, block_context):
        """
        RMA Чтение списка задач с Online
        VII Вынес формирование фильтра в блок, Рефакторинг
        :param context: Контекст с подключения context.report.params(Значение узла json_connection подключения ?)
        :type context: dict (?)
        :param block_context: Контекст операции (Значение узла Data соответствующего элемента таблицы ExtSyncDoc ?)
        :type block_context: dict (?)
        :return: Список изменившихся объектов
        :rtype: list
        """
        _filter = {
            'Навигация': {
                'РазмерСтраницы': str(context.report.params.get('page_size', 25)),
                'Страница': block_context.get('page', 0)
            },
            'Тип': 'Мои'
        }

        events = await context.report.service.task_list(context.report.params, _filter)
        return events.get('Реестр', [])
