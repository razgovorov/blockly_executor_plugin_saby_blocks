from blockly_executor.plugins.saby.connector.connector import Connector
from blockly_executor.core.helpers.xdto_serializer import XdtoSerializer


class Connector1C(Connector):
    async def run(self, data):
        serializer = XdtoSerializer()
        data = serializer.decode(data)

        result = await self.execute(**data)

        result = serializer.encode(result)

        return result
