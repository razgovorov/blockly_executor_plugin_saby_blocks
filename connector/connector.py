import logging
from urllib.parse import unquote


from blockly_executor import ExtException
from ..BlocklyContext import BlocklyContext
from blockly_executor.core.executor import BlocklyExecutor
from ..Report import Report

logger = logging.getLogger('Blockly')


class Connector:
    def __init__(self, service):
        self.service = service

    async def execute(self, **kwargs):
        try:
            connection_uuid = kwargs['operation']
            algorithm_uid = kwargs.pop('algorithm')
        except KeyError as err:
            raise ExtException(message='Неуказан обязательный параметр', detail=str(err))
        try:
            if not connection_uuid:
                raise Exception('"operation" not defined')
            connection = kwargs.get('connection') or self.service.connection_read(connection_uuid)

            report = Report(
                operation=connection,
                uuid=kwargs.get('uid', None),
                service=self.service,
            )
            await report.read()

            context = await BlocklyContext.init(
                report=report,
                uuid=kwargs.get('uid', None),
                service=self.service,
                workspace_name=algorithm_uid,
                **kwargs,
            )

            executor = BlocklyExecutor(
                logger=logger,
                # debug_mode=kwargs.get('debug_mode'),
                breakpoints=kwargs.get('breakpoints'),
            )
        except Exception as err:
            raise ExtException(parent=err)
        try:
            context = await executor.execute(
                context,
                endpoint=kwargs.get('endpoint'),
                commands_result=kwargs.get('commands_result'),
            )
        except Exception as ex:
            raise ExtException(parent=ex, action='Connector.executor.execute')
        await report.update(context)
        return context.to_result()
