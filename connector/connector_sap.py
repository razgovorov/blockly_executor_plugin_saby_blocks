from blockly_executor.core.helpers.sap_xml_serializer import SapXmlSerializer
from blockly_executor.plugins.saby.connector.connector import Connector


class ConnectorSap(Connector):

    @staticmethod
    def lower_params(data):
        lower_data = {}
        for key in data:
            lower_data[key.lower()] = data[key]
        return lower_data

    @staticmethod
    def upper_params(data):
        upper_result = {}
        for key in data:
            upper_result[key.upper()] = data[key]
        return upper_result

    async def run(self, data):
        # encode commands_result data
        serializer = SapXmlSerializer()
        _commands_result = data.pop('commands_result', [])
        commands_result = []
        for elem in _commands_result:
            commands_result.append({
                'uuid': elem['UUID'],
                'status': elem['STATUS'],
                'data': serializer.encode(elem['DATA'])
            })
        data['commands_result'] = commands_result

        result = await self.execute(**data)

        result = self.upper_params(result)

        upper_commands = []
        for i, elem in enumerate(result['COMMANDS']):
            upper_commands.append({
                'NAME': elem['name'],
                'PARAMS': serializer.encode(elem['params']),
                'UUID': elem.get('uuid', '')
            })
        result['COMMANDS'] = upper_commands
        result = serializer.encode(result)

        return result
